class Main { 
	static int main(string[] args) {
		int[] itens = {5,1,3,7,10,2,8,6,4,9};
		
		Sort.bubble_sort(itens);
		Sort.selection_sort(itens);
		Sort.insertion_sort(itens);
		return 0;
	}
}
