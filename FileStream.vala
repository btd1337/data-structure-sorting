class FileStream {
	public static void read_file() {
		File file = File.new_for_path ("data.txt");
		try {
			FileInputStream file_input_stream = file.read();
			DataInputStream data_input_stream = new DataInputStream (file_input_stream);
			string line;
	
			while ((line = data_input_stream.read_line ()) != null) {
				print ("%s\n", line);
			}
		} catch (Error e) {
			print ("Error: %s\n", e.message);
		}
	}
	
	public static void write_file() {
		string content = "Hi.there\nThis is a test";
		try{
			FileUtils.set_contents("data.txt", content);
		}catch(Error e){
			stderr.printf ("Error: %s\n", e.message);
		}
	}
}
