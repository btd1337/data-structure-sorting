class Sort : GLib.Object { 
	public Sort() {
		
	}

	public static void bubble_sort(int[] itens){
		int aux;
		bool swapped = true;
		for (int item = 0; item < itens.length - 1 && swapped ; item++) {
			swapped = false;
			for (int next_item = item + 1; next_item < itens.length; next_item++) {
				if (itens[item] > itens[next_item]) {
					aux = itens[next_item];
					itens[next_item] = itens[item];
					itens[item] = aux;
					swapped = true;
				}
			}
		}
		print_array(itens, "Bubble");
	}

	public static void selection_sort(int[] itens){
		int index_smallest;
		int aux;

		for (int index_item = 0; index_item < itens.length - 1; index_item++) {
			index_smallest = index_item;
			for (int index_comparate_item = index_item + 1; index_comparate_item < itens.length; index_comparate_item++) {
				if (itens[index_comparate_item] < itens[index_smallest]) {
					index_smallest = index_comparate_item;
				}
			}
			if (index_smallest != index_item) {
				aux = itens[index_item];
				itens[index_item] = itens[index_smallest];
				itens[index_smallest] = aux;
			}
		}
		print_array(itens, "Selection");
	}

	public static void insertion_sort(int[] itens){
		int index_lowest = 0;
		int current_value;
		bool swapped;

		for (int index_item = 1; index_item < itens.length - 1; index_item++) {
			swapped = false;
			for (int index_comparate_item = index_item - 1; index_comparate_item >= 0; index_comparate_item--) {
				if (itens[index_item] < itens[index_comparate_item]) {
					index_lowest = index_comparate_item;
					swapped = true;
				}
			}
			if (swapped) {
				current_value = itens[index_item];
				// (src, dest, length)
				itens.move (index_lowest, index_lowest + 1, index_item - index_lowest);	
				itens[index_lowest] = current_value;
			}
		}
		print_array(itens, "Insertion");
	}

	public static void print_array(int[] itens, string method_name){
		print("=== %s Sort ===\n", method_name);
		foreach (var item in itens) {
			print("%i ", item);
		}
		print("\n");
	}
}
